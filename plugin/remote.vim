let s:remote_cmd = "scp -o LOGLEVEL=error"

function! RemoteLinkCopy()
    execute "!" . s:remote_cmd expand("%") b:remote_fname
endfunction

function! RemoteLink(remote_fname)
    let b:remote_fname = a:remote_fname
    nnoremap <buffer> <localleader>c :call RemoteLinkCopy()<cr>
endfunction

function! s:remoteundiff()
    diffoff!
    echom "Deleteing " . expand("<afile>")
    call delete(expand("<afile>"))
    execute "autocmd!" "*" expand("<afile>")
endfunction

function! RemoteDiff(p)
    let l:tmpfile = tempname()
    echom "tempfile: " . l:tmpfile
    execute "autocmd" "BufEnter" l:tmpfile "setl bufhidden=delete readonly"
    execute "autocmd" "BufUnload" l:tmpfile "call <SID>remoteundiff()"
    exe "!" . s:remote_cmd b:remote_fname l:tmpfile
    execute a:p "diffs" l:tmpfile
endfunction

function! RemoteLinkRemove()
    unlet b:remote_fname
    nunmap <localleader>c
endfunction

command! RemoteLink execute "call" 'RemoteLink("' . expand(input("Enter remote name: ")) . '")'
command! RemoteLinkRemove call RemoteLink()
command! RemoteDiff execute "call" 'RemoteDiff("")'
command! RemoteDiffV execute "call" 'RemoteDiff("vert")'
